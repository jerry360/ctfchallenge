package com.example.ctf.IO;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.ctf.Settings;
import com.example.ctf.TriggerMe;
import com.google.gson.Gson;
import com.squareup.picasso.Downloader;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.OkHttpClient;


import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;



public class APIcalls {
    static Context context;
    static RequestQueue requestQueue;
    static String JWTsecret="FLAG{INTELEKTUALEC}";


    public static void getToken(Context contextCall){
        context=contextCall;
        String userJson=toJson(Settings.whoami);
        Log.d("GET TOKEN klic",userJson);
        JSONObject postparams = null;
        try {
            postparams = new JSONObject(userJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                Settings.urlIP+"/auth/getToken", postparams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d("GET TOKEN response", response.toString());
                            Settings.whoami.Token = response.get("token").toString();
                            Settings.whoami.Stuff = response.get("stuff").toString();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if(Settings.whoami.Stuff.contains("admin")){
                            Log.d("GET TOKEN", "pa te mam admin");
                            EventBus.getDefault().post(new TriggerMe(2));
                        }else{
                            EventBus.getDefault().post(new TriggerMe(1));
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("GET TOKEN error", error.toString());
                        EventBus.getDefault().post(new TriggerMe(-1));
                    }
                })
        {
            @Override
            public Map getHeaders (){
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                return headers;
            }

        };
        addToRequestQueue(jsonObjReq, "postRequest");

    }

    public static void register(Context contextCall){
        context=contextCall;
        String userJson=toJson(Settings.whoami);
        Log.d("GET TOKEN klic",userJson);
        JSONObject postparams = null;
        try {
            postparams = new JSONObject(userJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                Settings.urlIP+"/register", postparams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d("GET TOKEN response", response.toString());
                            Settings.whoami.Token = response.get("token").toString();
                            Settings.whoami.Stuff = response.get("stuff").toString();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        EventBus.getDefault().post(new TriggerMe(1));


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("GET TOKEN error", error.toString());
                        EventBus.getDefault().post(new TriggerMe(-1));
                    }
                })
        {
            @Override
            public Map getHeaders (){
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                return headers;
            }

        };
        addToRequestQueue(jsonObjReq, "postRequest");

    }

    public static <T> String toJson(T tmp) {
        Gson gson = new Gson();
        String rac_json=gson.toJson(tmp,tmp.getClass());
        return rac_json;
    }


    public static RequestQueue getRequestQueue() {
        if (requestQueue == null)
            requestQueue = Volley.newRequestQueue(context);

        return requestQueue;
    }


    public static void addToRequestQueue(Request request, String tag) {
        request.setTag(tag);
        getRequestQueue().add(request);

    }



    public static Picasso getPicasso(Context context) {
//        Picasso.Builder builder = new Picasso.Builder(context);
//
//        OkHttpClient client = new OkHttpClient();
//        client.networkInterceptors().add(new BasicAuthInterceptor());
//
//        Downloader downloader = new OkHttp3Downloader(client);
//
//        return builder.downloader(downloader).build();
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new BasicAuthInterceptor()).build();

//                .header("Authorization", "Bearer " + Settings.whoami.Token)
//                .url(Settings.urlIP+"/getImages/downloadImage/notFound.gif")
//                .build();
        return new Picasso.Builder(context).downloader(new OkHttp3Downloader(client)).build();
    }


}
