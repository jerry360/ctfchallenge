package com.example.ctf;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.ctf.IO.APIcalls;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class MainActivity extends AppCompatActivity {

    Button loginB, RegisterB;
    EditText pass,user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        EventBus.getDefault().register(this);
    }

    void init(){
        loginB=findViewById(R.id.LOGINbutton);
        pass=findViewById(R.id.passwordText);
        user=findViewById(R.id.usernameText);
        RegisterB=findViewById(R.id.RegisterButton);

        loginB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pass.getText().toString()!="" && user.getText().toString()!=""){
                    //base64
                    String encodedUser = Base64.getEncoder().encodeToString(user.getText().toString().getBytes());

                    Settings.whoami=new User(encodedUser,getSHA(pass.getText().toString()));
                    APIcalls.getToken(getApplicationContext());


                }
            }
        });



    RegisterB.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(pass.getText().toString()!="" && user.getText().toString()!=""){
                //base64
                String encodedUser = Base64.getEncoder().encodeToString(user.getText().toString().getBytes());

                Settings.whoami=new User(encodedUser,getSHA(pass.getText().toString()));
                APIcalls.register(getApplicationContext());


            }
        }
    });

}


    public static String getSHA(String input)
    {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger no = new BigInteger(1, messageDigest);
            String hashtext = no.toString(16);
//            while (hashtext.length() < 32) {
//                hashtext = "0" + hashtext;
//            }
            return hashtext;
        }
        catch (NoSuchAlgorithmException e) {
            System.out.println("ALGORITHM ERROR" + e);
            return null;
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRegisterEvent(TriggerMe event) {
        if(event.Message==1){
            Log.d("GET TOKEN","youve got it!!   "+Settings.whoami.Token);
            Intent i=new Intent(getApplicationContext(),Wellcome.class);
            startActivity(i);
        }else if(event.Message==-1){
            Log.d("GET TOKEN error","vrnil nulo");
        }else if(event.Message==2){
        Log.d("GET TOKEN","going admin");
        Intent i=new Intent(getApplicationContext(),webBrowser.class);
        startActivity(i);
        }
    }

}
