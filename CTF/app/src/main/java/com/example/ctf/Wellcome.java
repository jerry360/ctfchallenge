package com.example.ctf;

import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.example.ctf.IO.APIcalls;
import com.squareup.picasso.Picasso;

public class Wellcome extends AppCompatActivity {

    ImageView statusImg;
    TextView statusTxt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wellcome);
        init();
        //Uri uri= Uri.parse(Settings.urlIP+"/getImages/downloadImage/notFound.gif");
        //APIcalls.getPicasso(getApplicationContext()).load(uri).into(statusImg);

        String url = Settings.urlIP+"/getImages/downloadImage/notFound.gif";

        GlideUrl glideUrl = new GlideUrl(url,
                new LazyHeaders.Builder()
                        .addHeader("Authorization", "Bearer " + Settings.whoami.Token)
                        .build());

        Glide.with(this)
                .load(glideUrl)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(statusImg);

        statusTxt.setText("SI MISLU DA SI HEKER  AAH HHH!!! AH HAA HH AA HA     AAHA AHAA AH HHA HHH!!!     HHA HHH AHAA AHHH AAH AAHA AH AAA");
    }


    void init(){
        statusImg=findViewById(R.id.statusImgView);
        statusTxt=findViewById(R.id.statusText);
    }





}
