
package com.example.ctf;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

public class webBrowser extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_browser);

        Button Open;
        Open=findViewById(R.id.OpenButton);

        WebView webView=findViewById(R.id.webView);
        webView.loadUrl(Settings.vncurlIP);



        Open.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Settings.vncurlIP));
                startActivity(browserIntent);
            }
        });
    }
}
