package com.example.ctf;

public class User {
    public String Username;
    public String Password;
    public String Token;
    public String Stuff;

    public User(String username, String password) {
        Username = username;
        Password = password;
        Token = "null";
        Stuff = "null";
    }
}
