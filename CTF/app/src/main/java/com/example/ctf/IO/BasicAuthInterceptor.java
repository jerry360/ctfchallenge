package com.example.ctf.IO;

import android.view.textclassifier.TextLinks;

import com.example.ctf.Settings;

import java.io.IOException;

import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class BasicAuthInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {

        Request originalRequest = chain.request();

        Headers headers = new Headers.Builder()
                .add("Authorization", "Bearer " + Settings.whoami.Token)
                .add("User-Agent", "ctfMe")
                .build();

        Request newRequest = originalRequest.newBuilder()
                .headers(headers)
                .url(Settings.urlIP+"/getImages/downloadImage/notFound.gif")
                .build();

        Response response = chain.proceed(newRequest);

        return response;
    }
}
