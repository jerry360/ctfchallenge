package main


import (
	"fmt"
	"log"
	"database/sql"
	"io/ioutil"
	"bufio"
	"encoding/json"
	"net/http"
  "regexp"
	"os"
	"encoding/base64"
_ "github.com/go-sql-driver/mysql"

"github.com/gorilla/handlers"
"github.com/gorilla/mux"

jwtmiddleware "github.com/auth0/go-jwt-middleware"
"github.com/dgrijalva/jwt-go"

)

var mySecret=[]byte("FLAG{INTELEKTUALEC}")


type User struct{
  DbID string
  Username string
  Password string
  Stuff string
  Token string
}

var jwtMiddleware = jwtmiddleware.New(jwtmiddleware.Options{
	ValidationKeyGetter: func(token *jwt.Token) (interface{}, error) {
		return mySecret, nil
	},
	Extractor:     jwtmiddleware.FromFirst(jwtmiddleware.FromAuthHeader),
	UserProperty:  "user",
	SigningMethod: jwt.SigningMethodHS256,
})

var isValid = regexp.MustCompile(`^[0-9a-zA-Z]+$`).MatchString

func getToken(writer http.ResponseWriter, r *http.Request) {
	writer.Header().Set("Content-Type", "application/json")
	var user User
	_ = json.NewDecoder(r.Body).Decode(&user)
	user.Username=decodeMyBase(user.Username)
	fmt.Println("REQUEST FOR:  U="+user.Username + "    P=" + user.Password)
	if user.Username != "" && user.Password !="" && isValid(user.Password) && isValid(user.Username){
		db, err := sql.Open("mysql", "admin:goljufas@tcp(192.168.210.195:3306)/mydb")
		if err != nil {
			log.Fatal(err)
			panic(err.Error())
		}

		err = db.Ping()
		if err != nil {
			panic(err.Error())
		} else {
			fmt.Println("CONNECTED TO DATABASE!")
		}

		preperd, err := db.Prepare("select * from User where username=? and password=?")
		resault, err := preperd.Query(user.Username, user.Password)
		if err != nil {
			panic(err.Error())
		} else {
			for resault.Next() {
				var tmpuser User
				err = resault.Scan(&tmpuser.DbID, &tmpuser.Username, &tmpuser.Password, &tmpuser.Stuff)
				if err != nil {
					panic(err.Error())
				} else {
					fmt.Println("APPROVED!!!")
					token := jwt.New(jwt.SigningMethodHS256)
					claims := token.Claims.(jwt.MapClaims)
					claims["DbID"] = tmpuser.DbID
					claims["username"] = tmpuser.Username
					claims["password"] = tmpuser.Password
					claims["stuff"]=tmpuser.Stuff
					tokenString, _ := token.SignedString(mySecret)
					writer.Write([]byte("{ 'token' :'"+tokenString+"', 'stuff' : '"+tmpuser.Stuff+"' }"))
					defer db.Close()
					return
				}
			}
		}
	}
	fmt.Println("DENIED!!!")
	writer.WriteHeader(http.StatusUnauthorized)
}



func register(writer http.ResponseWriter, r *http.Request) {
	writer.Header().Set("Content-Type", "application/json")
	var user User
	_ = json.NewDecoder(r.Body).Decode(&user)
	user.Username=decodeMyBase(user.Username)
	fmt.Println("REGISTER REQUEST FOR:  U="+user.Username + "    P=" + user.Password)
	if user.Username != "" && user.Password !="" && isValid(user.Password) && isValid(user.Username){
		db, err := sql.Open("mysql", "admin:goljufas@tcp(192.168.210.195:3306)/mydb")
		if err != nil {
			log.Fatal(err)
			panic(err.Error())
		}

		err = db.Ping()
		if err != nil {
			panic(err.Error())
		} else {
			fmt.Println("CONNECTED TO DATABASE!")
		}

		preperd, err := db.Prepare("INSERT INTO User (username, password, stuff) VALUES (?, ?, 'user');")
		resault, err := preperd.Query(user.Username, user.Password)
		if err != nil {
			panic(err.Error())
		}else{
			var st = 0
			for resault.Next() {
				st++
			}
			if st > 1 {
				fmt.Println("napaka rows:", st)
				fmt.Print(st)
			} else {

					fmt.Println("APPROVED!!!")
					token := jwt.New(jwt.SigningMethodHS256)
					claims := token.Claims.(jwt.MapClaims)
					claims["username"] = user.Username
					claims["password"] = user.Password
					claims["stuff"]="user"
					tokenString, _ := token.SignedString(mySecret)
					writer.Write([]byte("{ 'token' :'"+tokenString+"', 'stuff' : 'user' }"))
					defer db.Close()
					return
				}
			}
}

	fmt.Println("DENIED!!!")
	writer.WriteHeader(http.StatusUnauthorized)
}




//keycloak!!!

func getMyImage(writer http.ResponseWriter, r *http.Request) {
	parameter := mux.Vars(r)
	var USERID string

	//type of user
	user := r.Context().Value("user")
	for k, v := range user.(*jwt.Token).Claims.(jwt.MapClaims) {
	fmt.Fprintf(os.Stdout, "%s :\t%#v\n", k, v)
	if k == "stuff" {
			USERID = v.(string)
		}
	}

	fmt.Println("GETING IMAGE FOR USER TYPE: ",USERID)
	if(USERID=="admin"){
		fmt.Println("///GET IMAGE WITH NAME:", parameter["id"])
			if parameter["id"]=="notFound.gif"{
				parameter["id"]="pepa.gif"
			}
			writer.Header().Set("Content-Type", "application/json")
			fmt.Println("///SENDING PEPAAAAAA!")
			f, _ := os.Open("UPLOADS/"+parameter["id"])

			// Read the entire JPG file into memory.
			reader := bufio.NewReader(f)
			content, _ := ioutil.ReadAll(reader)

			// Set the Content Type header.
			writer.Header().Set("Content-Type", "image/gif")

			// Write the image to the response.
			writer.Write(content)

		}else{
		fmt.Println("///GET IMAGE WITH NAME:", parameter["id"])
		writer.Header().Set("Content-Type", "application/json")

		f, _ := os.Open("./UPLOADS/notFound.gif")

		// Read the entire JPG file into memory.
		reader := bufio.NewReader(f)
		content, _ := ioutil.ReadAll(reader)

		// Set the Content Type header.
		writer.Header().Set("Content-Type", "image/jpeg")

		// Write the image to the response.
		writer.Write(content)
	}
}


func decodeMyBase(encoded string) string{
	decoded, err := base64.StdEncoding.DecodeString(encoded)
	if err != nil {
		fmt.Println("decode error:", err)
		return "error not a base64"
	}
	return string(decoded)
	// Output:
	// "some data with \x00 and \ufeff"
}




func main() {
	fmt.Println("[+]listening on port :8001")
	router := mux.NewRouter().StrictSlash(true)
	router.Use(func(h http.Handler) http.Handler { return handlers.LoggingHandler(os.Stdout, h) })
	router.Use(handlers.CORS())
	router.HandleFunc("/auth/getToken", getToken).Methods("POST")
	router.HandleFunc("/register", register).Methods("POST")
	imageRouter := router.PathPrefix("/getImages").Subrouter()
	imageRouter.Use(jwtMiddleware.Handler)
	imageRouter.HandleFunc("/downloadImage/{id}", getMyImage)
	log.Fatal(http.ListenAndServe(":8001", router))
}
